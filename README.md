# Pywal

Project forked from dylanaraps with a few personal additions.

--Added -C command to not create color schemes in ~/.cache/wal/

(usefull if you save current wallpaper in a cache and use wal on that file w/o
changing the name of the file. W/o this command the colorscheme would always 
stay the same)

Original project at: https://github.com/dylanaraps/pywal
